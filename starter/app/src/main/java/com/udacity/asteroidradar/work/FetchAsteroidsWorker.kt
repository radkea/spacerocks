package com.udacity.asteroidradar.work

import android.content.Context
import android.util.Log
import androidx.lifecycle.viewModelScope
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.udacity.asteroidradar.api.AsteroidApi
import com.udacity.asteroidradar.api.parseAsteroidsJsonResult
import com.udacity.asteroidradar.database.AsteroidDatabase
import com.udacity.asteroidradar.database.AsteroidDbEntity
import com.udacity.asteroidradar.repository.AsteroidRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException

class FetchAsteroidsWorker(appContext: Context, params: WorkerParameters):
    CoroutineWorker(appContext, params) {

    companion object{
        const val WORK_NAME = "FetchAsteroids"
    }

    override suspend fun doWork(): Result {

        var result = Result.success()
        val asteroidRepository = AsteroidRepository(AsteroidDatabase.getInstance(applicationContext))

        withContext(Dispatchers.IO) {
            try{
                asteroidRepository.refreshAsteroids()
            }catch(e: IOException){
                result = Result.retry()
            }
        }
        return result
    }
}