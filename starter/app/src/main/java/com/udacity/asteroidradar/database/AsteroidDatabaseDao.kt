package com.udacity.asteroidradar.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AsteroidDatabaseDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(asteroidList: List<AsteroidDbEntity>)

    @Query("select * from all_asteroids order by date asc")
    fun getAllAsteroids(): LiveData<List<AsteroidDbEntity>>

    @Query("select * from all_asteroids where asteroidId = :key")
    suspend fun getAsteroidId(key: Long): AsteroidDbEntity?

}