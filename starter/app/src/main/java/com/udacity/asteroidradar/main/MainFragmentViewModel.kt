package com.udacity.asteroidradar.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.udacity.asteroidradar.Asteroid
import com.udacity.asteroidradar.api.PicOfTheDay
import com.udacity.asteroidradar.api.PicOfTheDayApi
import com.udacity.asteroidradar.database.AsteroidDatabase
import com.udacity.asteroidradar.database.AsteroidDatabaseDao
import com.udacity.asteroidradar.repository.AsteroidRepository
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.Exception

//enum class ApiStatus{LOADING, ERROR, DONE}

class MainFragmentViewModel(val database: AsteroidDatabaseDao, application: Application) : ViewModel() {

    private val asteroidRepository = AsteroidRepository(AsteroidDatabase.getInstance(application))

    val asteroidsFromDb = database.getAllAsteroids()

    private val _picOfTHeDayResponse = MutableLiveData<PicOfTheDay>()
    val picOfTHeDayResponse
        get() = _picOfTHeDayResponse

    init{
        getImageOfTheDayData()
        getAsteroidData()
    }

    private fun getImageOfTheDayData(){
        viewModelScope.launch {
            try {
                _picOfTHeDayResponse.value = PicOfTheDayApi.moshiRetrofitService.getImageOfTheDay()
            }catch(e: Exception){
                Log.i("Yep", "yep failed")
            }
        }
    }

    // use the repository to fetch data
    private fun getAsteroidData(){
        viewModelScope.launch {
            try{
                asteroidRepository.refreshAsteroids()
            }catch(e:IOException){}
        }
    }

    fun onAsteroidClicked(id: Long){
        viewModelScope.launch {

            val asteroidDbEntity = database.getAsteroidId(id)

            _navigateToDetailFragment.value = Asteroid(
                    asteroidDbEntity?.asteroidId!!,
                    asteroidDbEntity?.name!!,
                    asteroidDbEntity?.date!!,
                    asteroidDbEntity?.magnitude!!,
                    asteroidDbEntity?.diameter!!,
                    asteroidDbEntity?.velocity!!,
                    asteroidDbEntity?.distance!!,
                    asteroidDbEntity?.hazardous!!
            )
        }
    }

    private var _navigateToDetailFragment = MutableLiveData<Asteroid>()
    val navigateToDetailFragment
        get() =  _navigateToDetailFragment
}

