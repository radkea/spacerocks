package com.udacity.asteroidradar.main

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.udacity.asteroidradar.R
import com.udacity.asteroidradar.database.AsteroidDbEntity

@BindingAdapter("textViewName")
fun TextView.setTextViewName(item: AsteroidDbEntity?) {
    item?.let{
        text = item.name
    }
}

@BindingAdapter("textViewDate")
fun TextView.setTextViewDate(item: AsteroidDbEntity?){
    item?.let{
        text = item.date
    }
}

@BindingAdapter("imageViewFace")
fun ImageView.setImageViewFace(item: AsteroidDbEntity?){
    item?.let{
        setImageResource(when (item.hazardous){
            true -> R.drawable.ic_status_potentially_hazardous
            else -> R.drawable.ic_status_normal
        })
    }
}