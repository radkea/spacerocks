package com.udacity.asteroidradar.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
//import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://api.nasa.gov/"

private val retrofit = Retrofit.Builder()
    .addConverterFactory(ScalarsConverterFactory.create())
    .baseUrl(BASE_URL)
    .client(
        OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)  //endpoint is literally taking forever, even in separate browser.....need to add visual indicator for user to show the app is not dead
            .readTimeout(20, TimeUnit.SECONDS)
            .build()
    )
    .build()

//private val parser = parseAsteroidsJsonResult()

object AsteroidApi{
    val retrofitService : AsteroidApiService by lazy{ retrofit.create(AsteroidApiService::class.java)}
}

interface AsteroidApiService {

    @GET("neo/rest/v1/feed")
    suspend fun getAsteroids(
        @Query("start_date") startDate: String,
//        @Query("end_date") endDate: String,
        @Query("api_key") apiKey: String = "Lo66WDgdXdsryA3CoMDyVwJbI0lWPLNlApHDfffC"
    ): String
}