package com.udacity.asteroidradar.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.udacity.asteroidradar.R
import com.udacity.asteroidradar.database.AsteroidDbEntity
import com.udacity.asteroidradar.databinding.ListItemAsteroidBinding

class MainFragmentRecyclerViewAdapter2(val clickListener: ItemClickListener) : ListAdapter<AsteroidDbEntity, ViewHolder>(AsteroidDiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener)
    }
}


class AsteroidDiffCallback: DiffUtil.ItemCallback<AsteroidDbEntity>() {
    override fun areItemsTheSame(oldItem: AsteroidDbEntity, newItem: AsteroidDbEntity): Boolean {
        return oldItem.asteroidId == newItem.asteroidId
    }

    override fun areContentsTheSame(oldItem: AsteroidDbEntity, newItem: AsteroidDbEntity): Boolean {
        return oldItem == newItem
    }
}



class ItemClickListener(val clickListener:(asteroidId: Long) -> Unit){
    fun onClick(asteroid: AsteroidDbEntity) = clickListener(asteroid.asteroidId)
}


class ViewHolder private constructor(val binding: ListItemAsteroidBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(item: AsteroidDbEntity, clickListener: ItemClickListener){
        binding.asteroidDbEntity = item
        binding.clickListener = clickListener
        binding.executePendingBindings()
    }

    companion object{
        fun from(parent: ViewGroup): ViewHolder{
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemAsteroidBinding.inflate(layoutInflater, parent, false)
            return ViewHolder(binding)
        }
    }
}