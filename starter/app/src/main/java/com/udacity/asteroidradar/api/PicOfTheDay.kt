package com.udacity.asteroidradar.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PicOfTheDay(
    val date: String,
    val explanation: String,
    val media_type: String,
    val service_version: String,
    val title: String,
    val url: String

)  : Parcelable{}