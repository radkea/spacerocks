package com.udacity.asteroidradar.main
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.ImageView
//import android.widget.TextView
//import androidx.recyclerview.widget.RecyclerView
//import com.udacity.asteroidradar.R
//import com.udacity.asteroidradar.database.AsteroidDbEntity
//
////class MainFragmentRecyclerViewAdapter: RecyclerView.Adapter<ViewHolder>() {
//class MainFragmentRecyclerViewAdapter(val clickListener: ItemClickListener):RecyclerView.Adapter<ViewHolder>() {
//
//    var data = listOf<AsteroidDbEntity>()
//    set(value) {
//        field = value
//        notifyDataSetChanged()
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val layoutInflater = LayoutInflater.from(parent.context)
//        val view = layoutInflater.inflate(R.layout.list_item_asteroid, parent, false)
//
//        return ViewHolder(view)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val item = data[position]
////        val res = holder.itemView.context.resources
//
//        holder.name.text = item.name
//        holder.date.text = item.date
//        if(item.hazardous) {
//            holder.icon.setImageResource(R.drawable.ic_status_potentially_hazardous)
//        }
//
//    }
//
//    override fun getItemCount(): Int {
//        return data.size
//    }
//}
//
//
//
//
//class ItemClickListener(val clickListener:(asteroidId: Long) -> Unit){
//    fun onClick(asteroid: AsteroidDbEntity) = clickListener(asteroid.asteroidId)
//
//}
//
//
//
//
//class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
//    val name: TextView = itemView.findViewById(R.id.textview_name)
//    val date: TextView = itemView.findViewById(R.id.textview_date)
//    val icon: ImageView = itemView.findViewById(R.id.imageview_face)
//}