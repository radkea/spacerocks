package com.udacity.asteroidradar.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="all_asteroids")
data class AsteroidDbEntity(

    @PrimaryKey//(autoGenerate = true)
    var asteroidId: Long = 0L,

    @ColumnInfo  //(name = "name")
    var name: String = "",

    @ColumnInfo  //(name = "date")
    var date: String = "",

    @ColumnInfo  //(name = "magnitude")
    var magnitude: Double = 0.0,

    @ColumnInfo  //(name = "diameter")
    var diameter: Double = 0.0,

    @ColumnInfo  //(name = "velocity")
    var velocity: Double = 0.0,

    @ColumnInfo  //(name = "distance")
    var distance: Double = 0.0,

    @ColumnInfo(name = "is_potentially_hazardous_asteroid")
    var hazardous: Boolean = false
)

