package com.udacity.asteroidradar.repository

import android.annotation.SuppressLint
import com.udacity.asteroidradar.Asteroid
import com.udacity.asteroidradar.api.AsteroidApi
import com.udacity.asteroidradar.api.parseAsteroidsJsonResult
import com.udacity.asteroidradar.database.AsteroidDatabase
import com.udacity.asteroidradar.database.AsteroidDbEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AsteroidRepository(private val database: AsteroidDatabase) {

        @SuppressLint("SimpleDateFormat")
        suspend fun refreshAsteroids(){
            withContext(Dispatchers.IO){
                var asteroids: String = ""
                try {
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    val currentDate = sdf.format(Date())

                    asteroids = AsteroidApi.retrofitService.getAsteroids(currentDate)
                } catch(e:Exception){}

                val asteroidList = parseAsteroidsJsonResult(JSONObject(asteroids))
                database.asteroidDatabaseDao.insert(asteroids.asAsteroidFormat(asteroidList))  //insert in DB
            }
        }

        fun String.asAsteroidFormat(receivedAsteroids: ArrayList<Asteroid>): List<AsteroidDbEntity>{
            val _entityList = ArrayList<AsteroidDbEntity>()

            for(it in receivedAsteroids){
                val entity = AsteroidDbEntity(
                    asteroidId = it.id,
                    name = it.codename,
                    date = it.closeApproachDate,
                    magnitude = it.absoluteMagnitude,
                    diameter = it.estimatedDiameter,
                    velocity = it.relativeVelocity,
                    distance = it.distanceFromEarth,
                    hazardous = it.isPotentiallyHazardous
                )
                _entityList.add(entity)
            }
            return _entityList
        }
}